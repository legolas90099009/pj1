https://bcho.tistory.com/815

https://aws.amazon.com/devops/what-is-devops/?nc1=h_ls

# DevOps

Development+Operation

기존 개발과 운영이 나뉘어서 개발/운영 되던 체계의 문제점을 해결하기 위해 만들어진 개발 문화/접근 방식이다.

## CI / CD(지속적 통합/전달)

devops 에서 지향하는 것으로 애플리케이션 개발 단계를 자동화 시켜 보다 짧은 주기로 배포(제공)하는 방식이다.

### CI(Continuous Integration)

**지속적 통합**은 자동화된 빌드/테스트가 수행된 후 개발자가 코드 변경 사항을 저장소에 통합하는 방식이다.
이를 통해 여러 명의 개발자가 동시에 애플리케이션 개발과 관련된 코드 작업을 할 시 생길 수 있는 충돌 문제를 해결 할 수 있다.

지속적 통합을 통해

* 개발자의 생산성이 향상되고
* 좀 더 잦은 테스트로 버그를 더 빠르게 발견/해결을 할 수 있고
* 배포가(업데이트가) 더 쉬워진다.

#### Jenkins

젠킨스는 이런 지속적 통합을 도와주는 CI 툴이다.

* 소스코드의 일관성을 유지
* 자동 빌드
  빌드 툴과의 연동이 되어 효율을 높일 수 있음
* 자동 테스트

### CD(Continuous Delivery)

**지속적 전달**은 빌드/테스트 이후 코드를 저장소에 릴리즈 하기 위한 코드 변경이 자동화 된 개발 방식이다.

빌드 이후 모든 코드 변경을 배포해 언제든 배포가 가능하게 만들어준다.

지속적 전달을 통해

* 소프트웨어 릴리스의 프로세스를 자동화 해서
* 개발자의 생산성이 향상되고 / 버그 해결이 빠르고 쉬워지며 / 업데이트가 쉬워진다.

