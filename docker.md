Docker

참조한 글들:

https://cloud.kt.com/portal/user-guide/education-eduadvanced-edu_adv_2

https://subicura.com/2017/01/19/docker-guide-for-beginners-1.html

https://kgh940525.tistory.com/entry/InfraStructure-Docker%EB%9E%80-%EB%AC%B4%EC%97%87%EC%9D%B8%EA%B0%80

https://goofcode.github.io/container-101

https://m.blog.naver.com/alice_k106/220218878967



* ## 도커란

* 컨테이너 기반의 오픈소스 가상화 플랫폼

* 기존엔 호스트 OS 위에 게스트 OS 전체를 가상화해서 사용했으나 이 경우 오버헤드(성능 손실)가 발생하게 됨

* 이를 해결하기 위해 가상화가 아닌 프로세스를 격리하는 방식을 사용하게 됨

* #### 리눅스 컨테이너

* 컨테이너 안에 가상공간을 만들지만 실행 파일을 호스트에서 직접 실행한다.(이 방식을 리눅스 컨테이너라고 부름)

<img src="https://subicura.com/assets/article_images/2017-01-19-docker-guide-for-beginners-1/vm-vs-docker.png" alt="가상머신과 도커" style="zoom: 33%;" />

* #### 이미지

* 이미지란 컨테이너 실행에 필요한 파일과 설정값등을 포함하고 있는것(서비스 운영에 필요한 서버 프로그램,소스코드,컴파일된 실행파일 등)이다.(이 이미지는 불변함)

<img src="https://subicura.com/assets/article_images/2017-01-19-docker-guide-for-beginners-1/docker-image.png" alt="Docker image" style="zoom: 33%;" />

* #### 도커 컨테이너

* 도커의 컨테이너는 여러 이미지들의 레이어로 이루어져 있다.
  * 자세한건 아래 링크 참조
    https://subicura.com/2017/01/19/docker-guide-for-beginners-1.html
    https://goofcode.github.io/container-101



* #### 도커 구조

  <img src="https://goofcode.github.io/assets/img/%E1%84%8F%E1%85%A5%E1%86%AB%E1%84%90%E1%85%A6%E1%84%8B%E1%85%B5%E1%84%82%E1%85%A5%E1%84%85%E1%85%A1%E1%86%AB%20%E1%84%86%E1%85%AE%E1%84%8B%E1%85%A5%E1%86%BA%E1%84%8B%E1%85%B5%E1%86%AB%E1%84%80%E1%85%A1%20101%20-%20%E1%84%8E%E1%85%A9%E1%84%89%E1%85%B5%E1%86%B7%E1%84%8C%E1%85%A1%E1%84%85%E1%85%B3%E1%86%AF%20%E1%84%8B%E1%85%B1%E1%84%92%E1%85%A1%E1%86%AB%20%E1%84%8F%E1%85%A5%E1%86%AB%E1%84%90%E1%85%A6%E1%84%8B%E1%85%B5%E1%84%82%E1%85%A5%20%E1%84%80%E1%85%B5%E1%84%89%E1%85%AE%E1%86%AF/8C99359E-097A-41A1-94D1-B968FF73DDCE.png" alt="img"  />

  * client: 유저가 명령어를 입력하면 서버(DOCKER_HOST)쪽에 전달을 해 준다.(docker daemon이 수행하게 됨)
  * DOCKER daemon이 registry 또는 repository (이미지 저장소) 로부터 이미지를 가져옴
  * 이 이미지들은 다시 데몬에 의해 부팅되어 컨테이너를 실행함

