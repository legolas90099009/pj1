https://cloud.kt.com/portal/user-guide/education-eduadvanced-edu_adv_3

https://bcho.tistory.com/1256

https://subicura.com/2019/05/19/kubernetes-basic-1.html

미완

# 쿠버네티스 개념잡기

클러스터는 컨테이너를 쉽고 빠르게 배포/확장하게 해주며 관리를 자동화 해주는 오픈소스 플랫폼이다.

## 쿠버네티스 기본 기능



1. #### 상태관리

2. #### 스케줄링

3. #### 클러스터

   * 가상 네트워크를 통해 하나의 서버에 있는 것처럼 통신
   * 클러스터는 마스터(클러스터 전체를 관리하는 컨트롤러)와 컨테이너가 배포되는 머신(가상머신이나 물리적인 서버머신)인 노드가 존재한다

4. #### 서비스 디스커버리

5. #### 리소스 모니터링

6. #### 롤아웃/롤백



### 오브젝트 

* 기본 오브젝트와 컨트롤러(기존 오브젝트를 생성하고 관리하는 추가적인 기능을 가짐)으로 이루어진다.
* 오브젝트의 스펙(설정)외에 추가정보인 메타 정보들로 구성이 된다

#### 오브젝트 스펙

* 오브젝트들은 모두 오브젝트의 특성(설정정보)을 기술한 오브젝트 스펙으로 정의가 되고 커맨드라인을 통해 오브젝트 생성시 인자로 전달하여 정의하거나 yaml, json 파일로 정의 할 수 있다.

#### 기본 오브젝트

* 쿠버네티스에 의해 배포 및 관리되는 가장 기본적인 오브젝트는 컨테이너화되어 배포되는 애플리케이션의 워크로드를 기술하는 오브젝트로 파드(컨테이너화 된 애플리케이션),서비스(로드밸런서),볼륨(디스크),네임스페이스(패키지명) 4가지가 있다.

* ##### 파드(Pod)

* 쿠버네티스에서 가장 기본적인 배포 단위로, 컨테이너를 포함하는 단위다.

* 쿠버네티스의 특징중 하나로 컨테이너를 개별적으로 하나씩 배포하는것이 아니라 파드라는 단위로 배포한다.(파드는 하나 이상의 컨테이너를 포함한다)

* 한 파드 내에 있는 컨테이너는 ip와 port를 공유한다.
  두개의 컨테이너가 한 파드를 통해 배포되면 localhost를 통해 통신이 가능하다.

* 파드 내에 배포된 컨테이너간에는 디스크 볼륨을 공유할 수 있다.(=컨테이너들끼리 다른 컨테이너의 파일을 읽어올 수 있다)

<img src="https://t1.daumcdn.net/cfile/tistory/9913C64D5B02D9C826" alt="img" style="zoom:50%;" />

* ##### 볼륨(Volume)

* 파드가 기동할 때 디폴트로, 컨테이너마다 로컬 디스크를 생성해서 기동되는데 이 로컬 디스크의 경우에는 영구적이지 못하다(=컨테이너가 리스타트 되거나 새로 배포될 때 마다 로컬 디스크는 파드의 설정에 따라서 새롭게 정의되고 배포되면 디스크에 기록된 내용이 유실됨)

* 데이터 베이스와 같이 영구적으로 파일을 저장해야 하는 경우를 위해 볼륨이라는 스토리지를 사용한다.
  (컨테이너의 외장 디스크에 비유)

* 파드가 기동할때 컨테이너에 마운트해서 사용함
  **마운트**(**mount**)는 컴퓨터 과학에서 저장 장치에 접근할 수 있는 경로를 디렉터리 구조에 편입시키는 작업을 말한다.

<img src="https://t1.daumcdn.net/cfile/tistory/99FC343C5B02D9C810" alt="img" style="zoom:50%;" />

<img src="https://t1.daumcdn.net/cfile/tistory/997CE9435B02D9C824" alt="img" style="zoom:50%;" />

* ##### 서비스(Service)

* 파드와 볼륨을 이용하여, 컨테이너들을 정의한 후에, 파드를 서비스로 제공할때, 일반적인 분산환경에서는 하나의 파드로 서비스 하는 경우는 드물고 , 여러개의 파드를 서비스하면서 이를 로드밸런서(서비스)를 이용해서 하나의 ip와 port로 묶어서 서비스를 제공한다.

* 파드의 경우엔 동적으로 생성이 되고, 장애가 생기면 자동으로 리스타트 되면서 ip가 바뀌기 때문에 로드밸런서에서 파드의 목록을 지정할 때 ip주소를 이용하는 것은 어렵다.

* 또한 오토스케일링으로 인해 파드가 동적으로 추가/삭제되기 때문에  추가/삭제된 파드목록을
  로드밸런서가 유연하게 선택 해 줘야한다.
  
* 이를 위해서 라벨과 라벨 셀렉터라는 개념을 사용한다.

<img src="https://t1.daumcdn.net/cfile/tistory/99B11D475B02D9C802" alt="img" style="zoom:50%;" />

* ##### 네임스페이스(Name space)

* 네임스페이스는 한 쿠버네티스 클러스터내의 논리적인 분리단위라고 보면 된다

* 파드,서비스 등은 네임스페이스별로 생성이나 관리가 될 수 있고 사용자의 권한도 네임스페이스별로 나눠서 부여가 가능하다.

  <img src="https://t1.daumcdn.net/cfile/tistory/999A364D5B02D9C834" alt="img" style="zoom:50%;" />

  * ###### 네임스페이스로 할 수 있는것

1. 사용자별로 네임스페이스별 접근 권한을 다르게 부여 할 수 있다
2. 네임스페이스별로 리소스의 쿼터(할당량)을 정할 수 있다.(네임스페이스별로 사용가능한 리소스의 수를 지정가능)
3. 네임스페이스별로 리소스를 나눠서 관리할 수 있다.(파드,서비스 등)
   * 참고-네임스페이스는 논리적인 분위 단위지 물리적이나 기타장치를 통해서 환경을 분리한것이 아니다. 다른 네임스페이스간의 pod라도 통신은 가능하다.
     높은 수준의 분리정책을 원할 경우 쿠버네티스 클러스터 자체를 분리하는것이 좋다.

* ### 라벨  

* ###### 라벨 셀렉터(label selector)

* 서비스를 정의할때 어떤 파드를 서비스로 묶을지 정의하는것을 라벨 셀렉터라고 한다.

* 각 파드를 생성할때 메타데이터 정보 부분에 라벨을 키/값 쌍으로 정의할 수 있다. 

* 서비스는 라벨 셀렉터에서 특정 라벨을 가지고 있는 파드만 선택해서 서비스에 묶게 된다.

* 라벨은 쿠버네티스의 리소스를 선택하는데 사용이 된다.

* 각 리소스는 라벨을 가질 수 있고 라벨 검색 조건에 따라 특정 라벨을 가지고 있는 리소스만을 선택 할 수 있다.
  이렇게 라벨을 선택해 특정 리소스만 배포하거나 업데이트 또는 선택된 리소스만 서비스에 연걸하거나 특정 라벨로 선택된 리소스에만 네트워크 접근 권한을 부여하는 행위 등을 할 수 있다.

### 컨트롤러

* 기본 오브젝트 4 개로 애플리케이션을 설정하고 배포하는것이 가능한데 이를 조금 더 편리하게 관리하게 하기 위해 컨트롤러라는 개념을 사용한다.

* 컨트롤러는 기본 오브젝트들을 생성하고 관리하는 역할을 해준다.

* 컨트롤러는 Replication Controller (aka RC), Replication Set, DaemonSet, Job, StatefulSet, Deployment 들이 있다.

* #### Replication Controller(RC)

<img src="https://t1.daumcdn.net/cfile/tistory/99D817375B02D9C805" alt="img" style="zoom:50%;" />

* Replication Controller는 지정된 숫자로 파드를 기동 시키고 관리하는 역할을 한다.

* Replication Controller는 크게 3가지 파트로 Replica의 수, Pod Selector, Pod Template 3가지로 구성된다.

  * Selector : Pod selector는 라벨을 기반으로 해 Replication Controller가 관리할 파드를 가지고 오는데 사용한다.
  * Replicas : Replication Controller에 의해서 관리되는 파드의 수인데, 그 숫자만큼 파드의 수를 유지하도록 한다.
    (Ex.Replicas의 수가 3개면 3개의 파드만 띄우고, 이보다 파드가 모자르면 새로운 파드를 띄우고 많으면 파드를 지움)
  * Template :파드를 추가로 기동할 때 이미지,포트,라벨 등의 정보를 어떻게 만들지에 대한 정보를 정의한다.

* 이미 돌고있는 파드가 있는 상태에서 Replication Controller 리소스를 생성하면 그 파드의 라벨이 Replication Controller의 라벨과 일치하면 새롭게 생성된 Replication Controller의 컨트롤을 받게 된다.
  만약 해당 파드들이 Replication Controller에서 정의한 Replica 수 보다 많으면 Replica 수에 맞게 추가분의 파드를 삭제하고 모자르면 Template에 정의된 파드 정보에 따라 새로 파드를 생성하는데 기존에 생성되어 있는 파드가 Template에 정의된 스펙과 달라도 그 파드를 삭제하지 않는다.
  <즉 기존의 파드들과 새 파드들의 Template에 정의된 스펙이 다르게 유지 될 수 있다>
(Ex.아파치 웹서버로 기동중이던 파드가 있고 Replication Controller의 Template는 nginx로 파드를 실행하게 되어 있다하더라도 기존에 돌고 있는 아파치 웹서버 기반의 파드를 삭제하지 않는다.)
  
* #### ReplicaSet

* ReplicaSet은 Replication Controller의 새 버전이라 생각하면 된다.

* Replication Controller 는 Equality 기반 Selector를 이용하는데 반해, Replica Set은 Set 기반의 Selector를 이용한다.

* #### Deployment

* Deployment는 Replication controller 와 Replica Set의 좀 더 상위 추상화 개념이다.

* ##### 쿠버네티스 배포

* 쿠버네티스의 Deployment 리소스를 이해하기 위해선 쿠버네티스에서 Deployment 없이 기존에 어떻게 배포를 하는지에 대해서 이해해야 한다.

* 