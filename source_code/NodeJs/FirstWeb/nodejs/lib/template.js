module.exports = {
    HTML:function(title, list, body, control){//각 요소들을 받아서 html 코드에 삽입
        return `
          <!doctype html>
          <html>
          <head>
            <title>WEB1 - ${title}</title>
            <meta charset="utf-8">
          </head>
          <body>
            <h1><a href="/">WEB</a></h1>
            ${list}
            ${control}
            ${body}
          </body>
          </html>
          `;
    },
    List:function(filelist){//filelist를 받아서 배열의 모든 요소를 목록으로 출력
        var list = '<ul>';
        var i = 0;
        while(i < filelist.length){
            list = list + `<li><a href="/?id=${filelist[i]}">${filelist[i]}</a></li>`;
            i = i + 1;
        }
        list = list+`</ul>`;
        return list;
    }


}