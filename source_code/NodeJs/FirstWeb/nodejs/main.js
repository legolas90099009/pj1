var http = require('http');
var fs = require('fs');
var url = require('url');
var qs = require('querystring')
var path= require('path')
const sanitizeHtml = require('sanitize-html');

var template = require(`./lib/template.js`);// export 한 모듈 template 를 불러옴

var app = http.createServer(function(request,response){//웹브라우저에 접속이 들어올 때 마다 이 함수가 호출되면서 request 와 response 가 전달됨
    var _url = request.url;// url을 저장
    /*
    url.parse(urlStr, [parseQueryString], [slashesDenoteHost])
    parseQueryString ture : url 객체의 query 속성을 객체 형식으로 가져옵니다.(querystring 모듈을 사용합니다.)
    parseQueryString false : url 객체의 query 속성을 문자열 형식으로 가져옵니다.
    slashesDenoteHost ture : urlStr이 '//foo/bar' 인 경우 foo는 host, /bar는 path로 인식합니다.
    slashesDenoteHost false : urlStr이 '//foo/bar' 인 경우 //foo/bar 전체를 path로 인식하고 host는 null 입니다.
    */
    var queryData = url.parse(_url, true).query; //url 객체의 query 부분을 객체로 저장함
    var pathname = url.parse(_url, true).pathname; // url 의 path 부분을 객체로 저장함
    if(pathname === '/'){ // url의 path부분이 '/' 일 경우(루트일 경우)
        if(queryData.id === undefined){// url의 query가 정의되자 않앗을 경우(메인 페이지인 경우)
            fs.readdir('./data', function(error, filelist){//data 디렉토리의 파일을 filelist에 배열로 저장함
                var title = 'Welcome';//본문의 제목
                var description = 'Hello, Node.js';//본문의 내용
                var list = template.List(filelist);//template module 참조
                var html = template.HTML(title, list, `<h2>${title}</h2>${description}`,' <a href="create">create</a>');//"
                response.writeHead(200);//응답 헤더
                response.end(html);//응답 본문
            })
        }
        else {//url의 query가 정의되어 있을경우(undefined가 아닐 경우)(data 디렉토리의 파일을:글을 보고있는 경우)
            fs.readdir('./data', function(error, filelist){//data 디렉토리의 파일목록을 filelist에 배열로 저장함
                var pathfilter = path.parse(queryData.id).base;//query 의 path(경로)부분에서 ../ 경로를 없엔 순수 파일명으로 변경해서 저장함

                fs.readFile(`data/${pathfilter}`, 'utf8', function(err, description){//data 디렉토리의 {pathfilter}라는 이름의 파일을 description에 저장함(본문)
                    var title = queryData.id;//query의 id 를 저장
                    var Ctitle = sanitizeHtml(title);//보안
                    var Cdescription = sanitizeHtml(description);//보안
                    var list = template.List(filelist);//template module 참조
                    var html = template.HTML(Ctitle, list,
                        `<h2>${Ctitle}</h2>${Cdescription}`,
                        ` <a href="create">create</a>
                                 <a href="update?id=${Ctitle}">update</a>
                                 <form action="delete_process" method="post">
                                    <input type="hidden" name="id" value="${Ctitle}">
                                    <input type="submit" value="delete">
                                 </form>`);//template module 참조
                    response.writeHead(200);//응답 헤더
                    response.end(html);//응답 본문
                });
            });
        }
    }
    else if(pathname === '/create'){//url의 path부분이 create일 경우(/create 페이지일 경우)
        fs.readdir('./data', function(error, filelist){//data 디렉토리의 파일목록을 filelist에 배열로 저장함
            var title = 'Web - create';//본문의 제목
            var description = `<form action="/create_process" method="post">
                               <p><input type="text" name="title" placeholder="제목"></p>
                               <p>
                               <textarea name="description" placeholder="내용"></textarea>
                               </p>
                               <p>
                               <input type="submit">
                               </p>
                               </form>`;//본문의 내용
            var list = template.List(filelist);//template module 참조
            var html = template.HTML(title, list, `<h2>${title}</h2>${description}`,' <a href="create">create</a>');//template module 참조
            response.writeHead(200);//응답 헤더
            response.end(html);//응답 본문
        })
    }
    else if(pathname === '/create_process'){//url의 path부분이 create_process일 경우(create 페이지에서 submit 버튼을 눌렀을 경우)
        var body='';//변수 body 정의
        request.on('data', function (data) {//전달받은 request 에서
                                            //웹 브라우저가 POST 방식으로 전성되는 데이터가 많을 경우를 대비해서 특정한 양의 데이터를 수신할 때 마다 callback 함수를 호출하고 data인자를 통해 수신한 데이터를 전송하게 돼 있음
            body = body + data;//함수가 호출 될 때 마다 body 에 data를 저장(입력)
        });
        request.on('end', function () {//더 이상 정보 수신이 없을 경우 이 함수가 호출됨
            var post = qs.parse(body);//post 정보 저장
            var title = post.title;//post 정보의 title 을 추출, 저장
            var description = post.description;//post 정보의 description 을 추출, 저장
            fs.writeFile(`data/${title}`, description, 'utf8', function (err) {//data 경로에 {title}의 이름으로 description(본문)의 내용을 저장
                response.writeHead(302, {Location: `/?id=${title}`});//작성한 글의 페이지로 이동시킴
                response.end('done');
            });
        });
    }
    else if(pathname === '/update'){//url의 path부분이 updata일 경우(업데이트 페이지)
        fs.readdir('./data', function(error, filelist){//data 디렉토리의 파일목록을 filelist에 배열로 저장함
            var pathfilter = path.parse(queryData.id).base;//query 의 path(경로)부분에서 ../ 경로를 없엔 순수 파일명으로 변경해서 저장함
            fs.readFile(`data/${pathfilter}`, 'utf8', function(err, description){//data 디렉토리의 {pathfilter}의 라는 이름의 파일을 description에 저장함(본문)
                var title = queryData.id;//query의 id를 저장
                var list = template.List(filelist);//template module 참조
                var description = `<form action="/update_process" method="post">
                                   <input type="hidden" name="id" value="${title}">
                                   <p><input type="text" name="title" placeholder="제목" value="${title}"></p>
                                   <p>
                                   <textarea name="description" placeholder="내용">${description}</textarea>
                                   </p>
                                   <p>
                                   <input type="submit">
                                   </p>
                                   </form>`;//본문의 내용(구성)
                var html = template.HTML(title, list, `<h2>${title}</h2>${description}`,
                    `<a href="/create">create</a> <a href="/update?id=${title}">update</a>`);//template module 참조
                response.writeHead(200);//응답 헤더
                response.end(html);//응답 본문
            });
        });
    }else if(pathname === '/update_process'){//url의 path부분이 updata_process일 경우(update 페이지에서 submit 버튼을 눌렀을 경우)
        var body='';//72line
        request.on('data', function (data) {//73, 74line
            body = body + data;//75line
        });
        request.on('end', function () {//77line
            var post = qs.parse(body);//post 정보 저장
            var id = post.id//post 정보의 id를 추출
            var title = post.title;//post 정보의 title 추출
            var description = post.description;//post 정보의 description 추출
            fs.rename(`data/${id}`, `data/${title}`, function (error) {//{id}라는 이름의 파일을 {title}의 이름으로 변경함
                fs.writeFile(`data/${title}`, description, 'utf8', function (err) {//{title}라는 이름의 파일을 생성/파일에 description 덮어쓰기
                    response.writeHead(302, {Location: `/?id=${title}`});//변경한 글의 페이지로 이동시킴
                    response.end();//응답 본문
                });
            })
            });

    }else if(pathname === '/delete_process'){//url의 path부분이 delete_process일 경우(delete 버튼을 눌렀을 경우)
        var body='';//72 line
        request.on('data', function (data) {//73, 74line
            body = body + data;//75line
        });
        request.on('end', function () {//77line
            var post = qs.parse(body);//post 정보 저장
            var id = post.id//post 정보의 id를 추출
            var pathfilter = path.parse(id).base;//id의 path부분에서 ../ 경로를 없엔 순수 파일명으로 변경해서 저장
            fs.unlink(`data/${pathfilter}`, function (error) {//{pathfilter}라는 이름의 파일을 삭제
                response.writeHead(302, {Location: `/`});//메인 페이지로 이동
                response.end();//응답 본문
            })
        });

    }else{
        response.writeHead(404);
        response.end('Not found');
    }


});
app.listen(3000);