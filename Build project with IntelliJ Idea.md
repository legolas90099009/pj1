빌드 도구

https://woowabros.github.io/tools/2019/04/30/gradle-kotlin-dsl.html 
우아햔형제들 기술 블로그 (빌드도구란, gradle, wrapper)

https://araikuma.tistory.com/460?category=782572 
devkuma gradle

https://galid1.tistory.com/196
build.gradle 분석

# Build project by IntelliJ Idea~~

## 빌드 도구

### 빌드 도구란

> 빌드 도구란 소스코드를 실행 가능한 애플리케이션 생성물을 자동으로 만드는 프로그램이다.
> 빌드 과정은 소스코드를 컴파일하고 연결하고 패키징하여 실행가능한 형태로 가공한다.

빌드 자동화는 개발자가 다음과 같은 일상적인 작업에서 수행하는 광범위한 작업을 자동화 시키는것이다.

1. 의존성 라이브러리 다운로드
2. (컴파일) 소스 코드를 이진 코드로 전환
3. 이진 코드 패키징
4. 테스트 실행
5. 운영 시스템 배포

##### 빌드 도구를 써야하는 이유

소규모 프로젝트에서 개발자들이 종종 빌드 프로세스를 수동으로 호출하는데, 대규모 프로젝트에선 빌드 과정에서 순서와 의존성을 추적하기 어려워서 빌드 도구를 사용해 일관성을 유지해야한다.

#### Maven

스프트웨어 프로젝트 관리도구.
POM의 개념에 기반해 프로젝트 빌드, 보고서 생성 및 문서화가 가능하다. 
그레이들도 메이븐의 POM(pom.xml 파일을 통해 메이븐 프로젝트를 xml 형식으로 표현한다.) 개념을 활용하고있다.

#### Gradle

gradle에서 메이븐과 다른 점은 하위 프로젝트에 대한 정의를 최상위 프로젝트에서 allprojects,subprojects 속성을 통해 각 프로젝트의 공통 속성과 작동을 정의 할 수 있다.

### 멀티 프로젝트(모듈)

프로젝트를 구성하다보면 공통된 기능과 코드를 하나의 모듈로 몰아넣고 다른 모듈에서 참조해 사용하는 방식을 사용하게 되는데 이는 멀티 프로젝트의 형식을 띄게 된다.
이런 방식을 gradle 에선 Cross project configuration 이라고 한다.

### 래퍼(Wrapper)

이전엔 각 개발자가 자신의 환경에 맞게 빌드도구를 설치해 실행환경설정, 관리를 해야 했다.
이후 빌드도구를 실행할 수 있는 jar 파일과 이를 실행할 수 있는 스크립트를 함께 등록해 관리하는 방식의 래퍼가 나오게 되었다.

Gradle wrapper 구성

```markdown
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar // 그레이들 래퍼 jar
│       └── gradle-wrapper.properties // 그레이들 래퍼 버전 및 실행환경 기록
├── gradlew // Unix 계열에서 실행가능한 스크립트
└── gradlew.bat // 윈도우에서 실행가능한 스크립트
```

Gradle wrapper 버전 변경 방법

~~~shell
// ./gradlew wrapper --gradle-version={version}
$ ./gradlew wrapper --gradle-version=5.4

// 이 글을 쓰고 있는 사이에 5.4.1 버전이 출시(2019-04-26)했다.
// 5.4 부터 JDK 12 지원한다. 내 프로젝트는 JDK 8에서 머물러 있는데...
~~~

이 명령으로 Gradle wrapper가 배포서버(Gradle Distribution Server)에서 해당 버전의 wrapper 바이너리 파일(gradle -wrapper.jar)을 지정된 위치에 다운받는다.

<img src="https://docs.gradle.org/current/userguide/img/wrapper-workflow.png" alt="wrapper workflow" style="zoom:67%;" />

wrapper를 사용하게 되면서 같은 프로젝트 작업자 로컬에 빌드도구를 설치하는 번거로움이 사라졌다.
wrapper의 버전을 업그레이드 하고 변경사항을 커밋하고 원격저장소에 푸시하면 팀원들에게 업그레이드 된 wrapper가 공유된다.

### build.gradle

Gradle에선 build.gradle 라는 파일에 빌드에 대한 처리를 작성한다.

```groovy
//task를 프로젝트에 추가시킴
plugins {
    id 'org.springframework.boot' version '2.3.5.RELEASE'
    id 'io.spring.dependency-management' version '1.0.10.RELEASE'
    id 'java'
}

group = 'com.zeronsoftn.cjm'//해당 라이브러리가 속해있는 기업 및 단체를 나타냄
version = '0.0.1-SNAPSHOT'//라이브러리의 버전
sourceCompatibility = '1.8'//java 8 jdk 를 사용해 자바 소스를 컴파일 하기 위해 삽입

//저장소를 지정하기 위한 문장이다.
repositories {
    //Apache Maven 중앙 저장소를 이용하기 위한 문장이다.
    mavenCentral()
}

//저장소에서 필요한 라이브러리를 사용할 수 있게 하는 문장이다.
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    runtimeOnly 'com.h2database:h2'
    testImplementation('org.springframework.boot:spring-boot-starter-test') {
        exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
}

//?/??/
test {
    useJUnitPlatform()//JUnit Platform 테스트를 실행하기 위해 native support를 제공하는데 이를 활성화 하기 위해 이 메소드를 호출해야함
}

```

