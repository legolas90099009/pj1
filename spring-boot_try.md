![image-20201112161806969](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112161806969.png)

![image-20201112161915565](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112161915565.png)

![image-20201112162125820](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112162125820.png)

![image-20201112162154014](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112162154014.png)

![image-20201112163246746](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112163246746.png)

```groovy
plugins {
    id 'org.springframework.boot' version '2.3.5.RELEASE'
    id 'io.spring.dependency-management' version '1.0.10.RELEASE'
    id 'java'
}

group = 'com.showexample.cjm'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '1.8'

repositories {
    mavenCentral()
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    testImplementation('org.springframework.boot:spring-boot-starter-test') {
        exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
}

test {
    useJUnitPlatform()
}

```

![image-20201112163300105](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112163300105.png)

```java
package com.showexample.cjm.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
```

![image-20201112163312872](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112163312872.png)

```java
package com.showexample.cjm.demo;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
public class SampleController {

    @RequestMapping
    @ResponseBody
    public String samplehome() {
        return "repeat";
    }
}
```

![image-20201112163322044](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112163322044.png)

![image-20201112163405207](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112163405207.png)

![image-20201112163430378](C:\Users\cjm\AppData\Roaming\Typora\typora-user-images\image-20201112163430378.png)

**SUCCESS!!**