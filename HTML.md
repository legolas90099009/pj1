공부중



정리중



미완성



참조한 글 : https://opentutorials.org/course/3084

# HTML

#### strong

태그 <.strong> contents <./strong> 으로 강조 표시해봄

~~~html
<strong>contents</strong>
~~~

<strong>contents</strong>





#### u

태그 <.u> contents <./u> 로 밑줄 표시 해 봄

~~~html
<u>contents</u>
~~~

<u>contents</u>





태그를 열고나서 <.태그명> 닫을땐 <./태그명> 을 사용함

검색으로 태그 알아볼 수 잇음(Ex.HTML h1 tag)





#### h

<.h1>  ~ <.h6> 태그는 HTML 헤드를 정의함(제목 태그)

~~~html
<h1>contents(h1)</h1>
<h2>contents(h2)</h2>
<h3>contents(h3)</h3>
<h4>contents(h4)</h4>
<h5>contents(h5)</h5>
<h6>contents(h6)</h6>
~~~

<h1>contents(h1)</h1>

-

<h6>contents(h6)</h6>





>어떤 정보를 알아내는데
>1분밖에 걸리지 않는다면
>그것은 그 정보를
>이미 알고 있는 것이나 다름없지 않을까요?
>
>생각해 보면 내 머릿속에 있는 지식도
>때에 따라서는 1분 이상 걸릴 때가 많고
>심지어 꺼내오는 것에 실패하는 경우도 많지 않나요?





#### br

HTML 에서 줄 바꿈을 하기 위해선 태그를 사용해야 함

태그 <.br> 를 사용하면 줄바꿈이 됨(빈 태그임)

~~~html
abc<br>def
~~~

abc<br>def





##### 빈 태그(빈 요소)

닫는 태그가 없음





#### p

태그 <.p> contents <./p> 를 사용하면 문단을 정의함 요소의 앞 뒤에 빈 줄을 자동으로 추가함

~~~html
<p>한글</p>
<p>eng</p>
~~~

<p>한글</p>

<p>eng</p>



css에 대해 따로 찾아볼것



```html
<p style="margin-top:45px;">
```

> 위와 같이 p 태그에
> style="margin-top:45px"를 추가하면
> p 태그의 위쪽에 45px 만큼의 여백(margin)이 생깁니다.
> 이것이 css입니다.



##### 요소

요소는 시작태그와 종료태그, 내용으로 구성됨
대부분의 요소는 중첩 될 수 있다
태그는 대소문자를 구분하지 않는다. 소문자 사용을 지향

태그 <.img> 이미지를 출력

~~~html
<img>
~~~

속성을 넣어줘야 함
(Ex.<.img src="(img link)::(img name)")

~~~html
<img src="https://s3-ap-northeast-2.amazonaws.com/opentutorials-user-file/module/3135/7648.png">
~~~

<img src="https://s3-ap-northeast-2.amazonaws.com/opentutorials-user-file/module/3135/7648.png">





##### 속성

source의 줄임말
태그 안에 들어감





##### 태그의 부모와 자식관계

~~~
<parent>
    <child></child>
</parent>
~~~

위 코드에서 parent 태그는 child 태그를 자식 태그, child 태그는 parent 태그를 부모 태그라고 한다.
꼭 태그들이 부모와 자식의 관계여야 할 필요는 없다.
하지만 몇몇 태그들은 부모와 자식의 관계처럼 관계가 고정인 태그들이 있다.



#### list

태그 <.li> contents <./li>를 사용하면 목차가 생성된다.

~~~html
<li>java</li>
<li>python</li>
<li>c ++</li>
~~~

#### ul

unordered list 의 약자로
여러 목차를 다른 목차와 구분 할 수 있게 경계를 만들어 줄 때 <.ul> contents <./ul> 를 사용한다.

~~~html
<ul>
    <li>java</li>
    <li>python</li>
    <li>c ++</li>
</ul>
<ul>
    <li>한글</li>
    <li>eng</li>
</ul>
~~~

<ul>
    <li>java</li>
    <li>python</li>
    <li>c ++</li>
</ul>

<ul>
    <li>한글</li>
    <li>eng</li>
</ul>







#### ol

ordered list 의 약자로
여러 목차를 다른 목차와 구분 할 수 있게 경계를 만들어 줄 때 <.ol> contents <./ol>를 사용한다.

~~~html
<ol>
    <li>java</li>
    <li>python</li>
    <li>c ++</li>
</ol>
<ol>
    <li>한글</li>
    <li>eng</li>
</ol>
~~~

<ol>
    <li>java</li>
    <li>python</li>
    <li>c ++</li>
</ol>
<ol>
    <li>한글</li>
    <li>eng</li>
</ol>





#### Title

![img](https://s3-ap-northeast-2.amazonaws.com/opentutorials-user-file/module/3135/7665.png)

웹 브라우저의 탭을 보면 모두 제목을 가지고 있다.
이 제목을 지정하기 위해서 <.title> contents <./title> 를 사용한다.
검색엔진이 웹페이지를 분석할 때 가장 중요하게 생각하는 태그로, 가급적 쓰는게 좋다.





#### meta

<img src="https://s3-ap-northeast-2.amazonaws.com/opentutorials-user-file/module/3135/7667.png" alt="img" style="zoom:67%;" />

에디터의 우측 하단에 UTF-8 방식으로 저장되었다는게 보인다.
하지만 웹 페이지를 UTF-8 방식으로 저장했다고 해도 웹페이지를 여는쪽은 이를 모른다.

이를 해결하기 위해서 <.meta> 태그를 이용해서 알려야 한다.

~~~html
<meta charset="utf-8">
~~~





#### head/body

본문을 설명하는 정보와 본문을 구분하기 쉽게 <.head> contents <./head> 와 <.body> contents <./body> 를 사용한다.

##### 추가로 !dotype 태그는 링크 참조 : https://opentutorials.org/course/3084/18409





#### anchor(a)

<.a> 태그를 사용하면 텍스트에 링크를 걸 수 있다.
HyperTest Reference(href) 속성이 필요하다.

~~~html
<a href="https://google.com" target="_blank" title="link to google">구글</a>
~~~

* target="_blank" 는 새창에서 페이지가 열리게 하는 속성이다.
* title 은 링크에 마우스를 가져다대면 보여지는 설명이다.

<a href="https://google.com" target="_blank" title="link to goole">구글</a>



웹 페이지를 링크로 묶어 만든 웹 페이지의 그룹을 흔히 웹 사이트라고 한다.



## 웹 사이트 만들기

웹 사이트를 만들 때는 어떻게 만들지 먼저 구상을 하고 순서를 정하는게 좋다.

링크 참조 : https://opentutorials.org/course/3084/18431

클라이언트 & 서버 : https://opentutorials.org/course/3084/18890

웹사이트 호스팅에 대한 글 : https://opentutorials.org/course/3084/18891

## 웹 서버

